/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "../../TP3/inc/main.h"

#include "../../TP3/inc/FreeRTOSConfig.h"
#include "board.h"

#include "FreeRTOS.h"
#include "task.h"

//Agrego el include de la libreria ciaaUART
#include "ciaaUART.h"
#include "semphr.h"
#include <stdbool.h>

/*==================[macros and definitions]=================================*/


//Defines de la configuracion serie
#define BAUDRATE 9600

//Defines de mensajes
#define MENSAJE_Inicio_Trama					"AA"
#define MENSAJE_Final_Trama						"55"
#define MENSAJE_Master_Inicial					strcat(strcat(MENSAJE_Inicio_Trama, "01"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV01			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "01"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV05			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "05"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV10			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "10"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV20			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "15"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ParadaMotor				strcat(strcat(MENSAJE_Inicio_Trama, "80"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_OK						strcat(strcat(MENSAJE_Inicio_Trama, "FF"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_NOK						strcat(strcat(MENSAJE_Inicio_Trama, "00"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_ErrorSinEnergia			strcat(strcat(MENSAJE_Inicio_Trama, "30"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_MotorFuncionando			strcat(strcat(MENSAJE_Inicio_Trama, "40"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_VelocidadIncorrecta		strcat(strcat(MENSAJE_Inicio_Trama, "50"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_EnMantenimiento			strcat(strcat(MENSAJE_Inicio_Trama, "60"), MENSAJE_Final_Trama)

//Defines para los leds
#define LED_ROJO	0
#define LED_VERDE	1
#define LED_AZUL	2
#define LED_1		3
#define LED_2		4
#define LED_3		5

#define ON	1
#define OFF	0


/*==================[internal data declaration]==============================*/

xSemaphoreHandle xBinSemaphoreInit;
xSemaphoreHandle xBinSemaphoreTx;
xSemaphoreHandle xBinSemaphoreRx;

bool flagOK;
bool flagNOK;
bool reset;

static uint32_t estadoTx = 0;
static uint32_t estadoRx = 0;

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

void vTaskInit( void * pvParameters);
void vTaskRx( void * pvParameters);
void vTaskTx( void * pvParameters);
void Reset(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

//Copiado de ciaaUART.c
void Mi_ciaaUART2Init(void)//Designare a UART2 como Master
{
	/* UART2 (USB-UART) */
	Board_UART_Init(LPC_USART2);
	Chip_UART_Init(LPC_USART2);
	Chip_UART_SetBaud(LPC_USART2, BAUDRATE);
	Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);//Agregando siguiendo el esquema de los board.c
	//Configuro los pines
	Chip_SCU_PinMux(1, 15, MD_PDN, FUNC1);              /* P1_15: UART2_TXD */
	Chip_SCU_PinMux(1, 16, MD_PLN|MD_EZI|MD_ZI, FUNC1); /* P1_16: UART2_RXD */
	//Configuro las FIFO
	Chip_UART_SetupFIFOS(LPC_USART2, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0);

	Chip_UART_TXEnable(LPC_USART2);
	Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);
	NVIC_EnableIRQ(USART2_IRQn);
}

void Mi_ciaaUART3Init(void)
{
	/* UART3 (RS232) */
	Board_UART_Init(LPC_USART3);
	Chip_UART_Init(LPC_USART3);
	Chip_UART_SetBaud(LPC_USART3, BAUDRATE);
	Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);//Agregando siguiendo el esquema de los board.c
	//Configuro los pines
	Chip_SCU_PinMux(2, 3, MD_PDN, FUNC2);              /* P2_3: UART3_TXD */
	Chip_SCU_PinMux(2, 4, MD_PLN|MD_EZI|MD_ZI, FUNC2); /* P2_4: UART3_RXD */
	//Configuro las FIFO
	Chip_UART_SetupFIFOS(LPC_USART3, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0);

	Chip_UART_TXEnable(LPC_USART3);
	Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);
	NVIC_EnableIRQ(USART3_IRQn);
}

static void initHardware(void)
{
    SystemCoreClockUpdate();

    Board_Init();

    //Agrego la inicializacion de la UART de la CIAA
    Mi_ciaaUART2Init();
    Mi_ciaaUART3Init();

    Board_LED_Set(0, false);
}


/*==================[internal functions definition, definicion de tareas]====*/


static void task_LED(void * a)
{
	while (1) {
		Board_LED_Toggle(LED_2);
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}


void vTaskInit( void * pvParameters)
{
	xSemaphoreTake( xBinSemaphoreInit, 0 );
	for(;;)
	{
		Reset();
		Board_UARTPutSTR(MENSAJE_Master_Inicial);
		xSemaphoreGive( xBinSemaphoreRx );
		xSemaphoreTake( xBinSemaphoreInit, portMAX_DELAY );
	}
}


void vTaskRecibe( void * pvParameters)
{
	uint8_t datoLeido; // se almacena temporalmente el dato leido por puerto serie

	xSemaphoreTake( xBinSemaphoreRx, portMAX_DELAY );
	for(;;)
	{

		datoLeido = Board_UARTGetChar();
		if(datoLeido != 255)
		{
			switch(estadoRx)
			{
			case 0:
				if(datoLeido == 'A')
				{
					estadoRx = 1;
				}
				else
				{
					estadoRx = 0;
				}
				break;

			case 1:
				if(datoLeido == 'A')
				{
					estadoRx = 2;
				}
				else
					estadoRx = 0;
				break;

			case 2:
				if(datoLeido == 'F')
				{
					estadoRx = 3;
				}
				else if(datoLeido == '0')
					estadoRx = 4;
				else
					estadoRx = 0;
				break;

			case 3:

				if(datoLeido == 'F')
				{
					estadoRx = 5;
					flagOK = true;
				}
				else
					estadoRx = 0;

				break;

			case 4:

				if(datoLeido == '0')
				{
					estadoRx = 5;
					flagNOK = true;
				}
				else
					estadoRx = 0;
				break;

			case 5:
				if(datoLeido == '5')
				{
					estadoRx = 6;
				}
				else
					estadoRx = 0;
				break;

			case 6:
				if(datoLeido == '5')
				{
					estadoRx = 0;
					if(flagOK || flagNOK)
					{
						xSemaphoreGive( xBinSemaphoreTx );
						xSemaphoreTake( xBinSemaphoreRx, portMAX_DELAY );

					}
				}
				else
					estadoRx = 0;
				break;

			default:
				estadoRx = 0;
			} // end switch
		} // end if

		if(reset && flagOK)
			xSemaphoreGive( xBinSemaphoreInit );
	} // end for
}


void vTaskEnvia( void *pvParameters)
{
	xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );

	for(;;)
	{
		switch(estadoTx)
		{
		case 0:
			Board_UARTPutSTR(MENSAJE_Master_ArranqueMotorV01);
			estadoTx = 1;

			break;
		case 1:
			Board_UARTPutSTR(MENSAJE_Master_ArranqueMotorV05);
			estadoTx = 2;
			break;
		case 2:
			Board_UARTPutSTR(MENSAJE_Master_ArranqueMotorV10);
			estadoTx = 3;
			break;
		case 3:
			Board_UARTPutSTR(MENSAJE_Master_ArranqueMotorV20);
			estadoTx = 4;
			break;
		case 4:
			Board_UARTPutSTR("AA8055");
			reset = true;
			estadoTx = 0;
		break;

		default:
			estadoTx = 0;

		} // end switch

		xSemaphoreGive( xBinSemaphoreRx );
		xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );

	} // end for
}


void Reset(void)
{
	flagOK = false;
	flagNOK = false;
	estadoTx = 0;
	estadoRx = 0;
	reset = false;
}


/*==================[external functions definition]==========================*/

int main(void)
{
	initHardware();

	vSemaphoreCreateBinary( xBinSemaphoreInit );
	vSemaphoreCreateBinary( xBinSemaphoreRx );
	vSemaphoreCreateBinary( xBinSemaphoreTx );

	xTaskCreate(task_LED, (const char *)"task_LED", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(vTaskInit, (const signed char *)"Init", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+3, 0);
	xTaskCreate(vTaskRecibe, (const signed char *)"Recibe", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);
	xTaskCreate(vTaskEnvia, (const signed char *)"Envia", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);



	vTaskStartScheduler();

	while (1) {
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
